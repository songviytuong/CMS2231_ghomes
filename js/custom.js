(function($){
    "use strict";
    $(document).ready(function() {
		jQuery('.registerbutton').click(function(){
			jQuery(this).parent().next('div.formregister').toggle();
		});
		var widthbody = jQuery('body').width();
		var widthform = jQuery('.form_showhide').width();
		var magin_form = (widthbody - widthform)/4;
		jQuery('.icon_img,p.clouse').click(function(){
			jQuery('.form_showhide,.mask').toggle(
				400,
				function(){
					jQuery('.form_showhide').animate({'left':magin_form});
					return false;
				}
			);
		});
		setInterval(function(){
			jQuery('.icon_img .before').toggleClass('hideimg');
		},600);
		jQuery('.mask,.clouseimg').click(function(){
			jQuery('.hide_content,.form_showhide,.mask').hide();
			//jQuery(this).hide();
		});
		
		jQuery('.close').click(function(){
			jQuery('#page-popup').hide();
		});
    });

})(jQuery);