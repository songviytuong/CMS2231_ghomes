<?php
#(c)2015 by Lee Peace (songviytuong@gmail.com)
#Visit my homepage at: http://www.idev.com.vn

function smarty_function_cms_copyright_link($params, $smarty) {

    global $CMS_COPYRIGHT_LINK;
	
	if( isset($params['assign']) ){
		$smarty->assign(trim($params['assign']),$CMS_COPYRIGHT_LINK);
		return;
	}
	
    return $CMS_COPYRIGHT_LINK;
}

function smarty_cms_about_function_cms_copyright_link() {
?>
	<p>Author: Lee Peace &lt;songviytuong@gmail.com&gt;</p>
	<p>Version: 1.0</p>
	<p>
	Change History:<br/>
	None
	</p>
<?php
}
?>