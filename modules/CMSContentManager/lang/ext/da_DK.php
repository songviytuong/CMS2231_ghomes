<?php
$lang['addcontent'] = 'Tilføj nyt indhold';
$lang['apply'] = 'Aktivér';
$lang['bulk_active'] = 'Sæt aktiv';
$lang['bulk_cachable'] = 'Sæt cache-bar';
$lang['bulk_changeowner'] = 'Skift ejer';
$lang['bulk_delete'] = 'Slet';
$lang['bulk_hidefrommenu'] = 'Skjul i menu';
$lang['bulk_inactive'] = 'Sæt inaktiv';
$lang['bulk_insecure'] = 'Sæt usikker (HTTP)';
$lang['bulk_noncachable'] = 'Sæt ikke cache-bar';
$lang['bulk_secure'] = 'Sæt sikker (HTTPS)';
$lang['bulk_setdesign'] = 'Sæt design & skabelon';
$lang['bulk_showinmenu'] = 'Vis i menu';
$lang['cancel'] = 'Afbryd';
$lang['close'] = 'Luk';
$lang['colhdr_id'] = 'Id';
$lang['coltitle_id'] = 'Det numeriske id for indholdssiden';
$lang['colhdr_active'] = 'Aktiv';
$lang['coltitle_active'] = 'Aktiv betyder at besøgende overhovet kan se siden';
$lang['colhdr_alias'] = 'Alias';
?>